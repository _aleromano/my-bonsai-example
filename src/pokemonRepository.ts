
export type PokemonRepository = () => Promise<number>
export type PokemonRepositoryFactory = () => PokemonRepository;

export const createPokemonRepository: PokemonRepositoryFactory = () => async () => {
    const network = await fetch('/pokemon');
    const response = await network.json();
    return response.count;
};