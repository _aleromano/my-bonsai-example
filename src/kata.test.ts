type RomanToDecimal = {
  [key: string]: number;
};

const MAP_ROMAN_TO_DECIMAL: RomanToDecimal = {
  M: 1000,
  C: 100,
  L: 50,
  X: 10,
  V: 5,
  I: 1,
};

const convertRomanToDecimal = (roman: string) => {
  // @ts-ignore
  const romanDigit: string[] = [...roman];

  const decimalDigits = romanDigit.map(
    (decimalDigit) => MAP_ROMAN_TO_DECIMAL[decimalDigit]
  );

  if (decimalDigits.length === 1) {
    return MAP_ROMAN_TO_DECIMAL[roman] || 0;
  }

  if (decimalDigits.length === 2) {
    return decideToAddOrSubtract(decimalDigits[0], decimalDigits[1]);
  }

  let accumulator = 0;

  for (let i = 0; i < decimalDigits.length; i++) {
    const digit = decimalDigits[i];

    console.log(
      "ACC: ",
      accumulator,
      " DIGIT: ",
      digit,
      " DIGIT+1: ",
      decimalDigits[i + 1]
    );

    if (decimalDigits[i + 1] !== undefined) {
      if (digit >= decimalDigits[i + 1]) {
        accumulator = decideToAddOrSubtract(accumulator, digit);
      } else {
        accumulator =
          accumulator + decideToAddOrSubtract(digit, decimalDigits[i + 1]);
        i += 1;
      }
    } else {
      accumulator = decideToAddOrSubtract(accumulator, digit);
    }
  }

  return accumulator;
};

function decideToAddOrSubtract(first: number, second: number) {
  if (first < second) {
    return second - first;
  }

  return first + second;
}

test("Given X it returns 10", () => {
  expect(convertRomanToDecimal("X")).toBe(10);
});

test("Given IX it returns 9", () => {
  expect(convertRomanToDecimal("IX")).toBe(9);
});

test("Given XL it returns 40", () => {
  expect(convertRomanToDecimal("XL")).toBe(40);
});

test("Given XC it returns 90", () => {
  expect(convertRomanToDecimal("XC")).toBe(90);
});

test("Given MC it returns 1100", () => {
  expect(convertRomanToDecimal("MC")).toBe(1100);
});

test("Given MXC it returns 1090", () => {
  expect(convertRomanToDecimal("MXC")).toBe(1090);
});

test("Given VII it returns 7", () => {
  expect(convertRomanToDecimal("VII")).toBe(7);
});

test("Given VIII it returns 8", () => {
  expect(convertRomanToDecimal("VIII")).toBe(8);
});

test("Given MCMIV it returns 1904", () => {
  expect(convertRomanToDecimal("MCMIV")).toBe(1904);
});
