export const createPokemonRepository = () => async () => {
    const response = await fetch('https://pokeapi.co/api/v2/pokemon');
    return response.json();
};