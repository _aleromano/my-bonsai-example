import React from 'react';
import { render, fireEvent, wait } from '@testing-library/react';
import App from './App';
import { createPokemonRepository } from './pokemonRepository';

test('renders learn react link', () => {
  const { getByText } = render(<App pokemonRepository={() => {}}/>);

  const button = getByText(/Increase/i);
  
  expect(button).toBeInTheDocument();
});

test('on clicking it increase the counter', () => {
  const { getByText } = render(<App pokemonRepository={() => {}}/>);
  const counter = getByText(/Counter is/i);
  const button = getByText(/Increase/i);
  
  fireEvent.click(button);
  
  expect(counter).toHaveTextContent('Counter is: 1');
});


test('on clicking on pokemon button it show the number of pokemon', async () => {
  const { getByText } = render(<App pokemonRepository={createPokemonRepository()}/>);
  const pokeButton = getByText(/Call Pokemon/i);
  
  fireEvent.click(pokeButton);
  
  await wait(() => expect(getByText(/Counter is/i)).toHaveTextContent('Counter is: 964'));
});


test('then clicking again on increase, increase it', async () => {
  const { getByText } = render(<App pokemonRepository={createPokemonRepository()}/>);
  const pokeButton = getByText(/Call Pokemon/i);
  const button = getByText(/Increase/i);
  
  fireEvent.click(pokeButton);
  
  await wait(() => expect(getByText(/Counter is/i)).toHaveTextContent('Counter is: 964'));
  
  fireEvent.click(button);
  
  expect(getByText(/Counter is/i)).toHaveTextContent('Counter is: 965');
});
