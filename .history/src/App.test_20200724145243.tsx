import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App pokemonRepository={() => {}}/>);

  const button = getByText(/Cloud Academy/i);
  
  expect(button).toBeInTheDocument();
});

test('on clicking it increase the counter', () => {
  const { getByText } = render(<App pokemonRepository={() => {}}/>);
  const counter = getByText(/Counter is/i);
  const button = getByText(/Cloud Academy/i);
  
  fireEvent.click(button);
  
  expect(counter).toHaveTextContent('Counter is: 1');
});


test('on clicking on pokemon button', () => {
  const { getByText } = render(<App pokemonRepository={() => {}}/>);
  const counter = getByText(/Counter is/i);
  const pokeButton = getByText(/Call Pokemon/i);
  
  fireEvent.click(button);
  
  expect(counter).toHaveTextContent('Counter is: 987');
});
