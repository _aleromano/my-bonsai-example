import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

import {Button, Text} from '@cloudacademy/bonsai';
import { Typography } from '@cloudacademy/bonsai/dist/theme/enums';

function App() {
  const [counter, setCounter] = useState(0);

  const handleClick = () => setCounter(counter + 1);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <Button label="from Cloud Academy" onClick={handleClick}></Button>
        <Text typography="caption"></Text>
      </header>
    </div>
  );
}

export default App;
