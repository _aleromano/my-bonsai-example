
export type PokemonRepository = () => Promise<number>
type PokemonRepositoryFactory = () => PokemonRepositoryexport ;

export const createPokemonRepository: PokemonRepositoryFactory = () => async () => {
    const network = await fetch('/pokemon');
    const response = await network.json();
    return response.count;
};