import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  const { getByText } = render(<App />);

  const button = getByText(/Cloud Academy/i);
  
  expect(button).toBeInTheDocument();
});

test('on clicking it increase the counter', () => {
  const { getByText } = render(<App />);
  const counter = getByText(/Counter is/i);
  const button = getByText(/Cloud Academy/i);
  
  fireEvent(button, new Event('click'));
  
  expect(counter).toHaveTextContent('Counter is: 1');
});
