const MAP_ROMAN_TO_DECIMAL = {
    X: 10,
    I: 1
}

const convertRomanToDecimal = (roman: string) => {
  if (roman === "X") return 10;

  return 0;
};

test("Given X it returns 10", () => {
  expect(convertRomanToDecimal("X")).toBe(10);
});

test("Given X it returns 10", () => {
    expect(convertRomanToDecimal("IX")).toBe(9);
  });
