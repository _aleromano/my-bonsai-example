
type PokemonRepositoryFactory = () => () => Promise<number>;

export const createPokemonRepository: PokemonRepositoryFactory = () => async () => {
    const network = await fetch('/pokemon');
    const response = await network.json();
    return response.count;
};