const MAP_ROMAN_TO_DECIMAL = {
  X: 10,
  I: 1,
};

const convertRomanToDecimal = (roman: string) => {
  // @ts-ignore
  const romanDigit = [...roman];

  const decimalDigits = romanDigit.map(
    (decimalDigit) => MAP_ROMAN_TO_DECIMAL[decimalDigit]
  );

  return decimalDigits.length === 1
    ? MAP_ROMAN_TO_DECIMAL[roman] || 0
    : decimalDigits[1] - decimalDigits[0];
};

test("Given X it returns 10", () => {
  expect(convertRomanToDecimal("X")).toBe(10);
});

test("Given X it returns 10", () => {
  expect(convertRomanToDecimal("IX")).toBe(9);
});
