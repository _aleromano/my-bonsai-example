export const createPokemonRepository = () => async () => {
    const response = await fetch('/pokemon');
    return response.json();
};