import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

import {Button, Text, theme} from '@cloudacademy/bonsai';

export const App: React.FC<{
  pokemonRepository: Function;
}> = ({pokemonRepository}) => {
  const [counter, setCounter] = useState(0);

  const handleClick = () => setCounter(counter + 1);

  const callPokemon = async () => {
    const pokeResponse = await pokemonRepository();
    console.log(pokeResponse)
    setCounter(pokeResponse.count);
  };


  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <Button label="from Cloud Academy" onClick={handleClick}></Button>
        <Text typography={theme.Typography.title} children={`Counter is: ${counter}`} palette={theme.Palette.twitter}></Text>

        <Button label="Call Pokemon" onClick={callPokemon}></Button>
      </header>
    </div>
  );
}

export default App;
