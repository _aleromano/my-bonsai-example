import { rest } from 'msw'
import pokemonResponse from './pokemonResponse.json';

export const handlers = [
    // // Handles a POST /login request
    // rest.post('/login', null),
    // Handles a GET /user request
    // rest.get('https://pokeapi.co/api/v2/pokemon', (req, res, ctx) => {
    rest.get('/pokemon', (req, res, ctx) => {
        return res(ctx.status(200), ctx.json(pokemonResponse))
    }),
  ];